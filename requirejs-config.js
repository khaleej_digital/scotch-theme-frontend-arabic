var config = {
    config: {
        mixins: {
            'Magento_Catalog/js/related-products': {
                'Magento_Catalog/js/related-products-mixin': true
            },
            'Magento_Catalog/js/upsell-products': {
                'Magento_Catalog/js/upsell-products-mixin': true
            }
        }
    }

};
