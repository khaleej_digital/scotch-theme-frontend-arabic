/**
 * Created by sz on 16.8.7.
 */

define(
    [
    'jquery',
    'mage/translate',
    'mage/dialog'
    ], function ($) {
        'use strict';

        $.widget(
            'os.switcherPopup', {
                _dialogData: {
                    title: '',
                },

                _state: {
                    isConfirmed: false
                },

                _create: function() {
                    this._dialogData.title = $('#switcherDialog').data('title');
                    this.element.on('click', $.proxy(this._openPopupWindow, this));
                },

                _openPopupWindow: function(event) {
                    
                    event.preventDefault();

                    // If OK button clicked, then redirect
                    if (this._state.isConfirmed) {
                        return true;
                    }

                    var self = this;
                    $('#switcherDialog').dialog(
                        {
                            title: this._dialogData.title,
                            modal: true,
                            draggable: false,
                            resizeable: false,
                            buttons: [
                            {
                                text: $.mage.__("لا"),
                                click: function() {
                                    $(this).dialog("close");
                                    $('#switcher-website-trigger').trigger('click');
                                }
                            },
                            {
                                text: $.mage.__("نعم"),
                                click: function() {
                                    $(this).dialog("close");
                                    self._state.isConfirmed = true;
                                    $(self.element).trigger('click');
                                }
                            }
                            ]
                        }
                    );

                    $('#switcherDialog')
                    .parents('.ui-dialog')
                    .find('.ui-dialog-titlebar-close')
                    .on(
                        'click',function() {
                            $('#switcher-website-trigger').trigger('click');
                        }
                    );

                    return false;
                }
            }
        );

        return $.os.switcherPopup;
    }
);