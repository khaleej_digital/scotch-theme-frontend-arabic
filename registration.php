<?php
/**
 * Copyright © Redbox Digital Limited. All rights reserved.
 */

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::THEME, 'frontend/Beside/arabic', __DIR__);
