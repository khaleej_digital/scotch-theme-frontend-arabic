/**
 * Copyright © Redbox Digital. All rights reserved.
 */

define([
    'jquery',
    'slick'
], function ($) {
    'use strict';

    return function (upsellProducts) {
        $.widget('mage.upsellProducts', $['mage']['upsellProducts'], {
            _create: function () {

                this._super();

                $(".products-upsell ol.items").slick({
                    mobileFirst: true,
                    slidesToShow:1,
                    slidesToScroll: 1,
                    arrows: false,
                    rtl: true,
                    responsive: [
                        {
                            breakpoint: 768,
                            settings: {
                                slidesToShow: 4,
                                slidesToScroll: 1,
                                arrows: true
                            }
                        }
                    ]
                });

                return;
            }
        });
        return $['mage']['upsellProducts'];
    };

});