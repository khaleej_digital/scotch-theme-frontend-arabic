/**
 * Copyright © Redbox Digital. All rights reserved.
 */

define([
    'jquery',
    'slick'
], function ($) {
    'use strict';

    return function (relatedProducts) {
        $.widget('mage.relatedProducts', $['mage']['relatedProducts'], {
            _create: function () {

                this._super();

                $(".products-related ol.items").slick({
                    mobileFirst: true,
                    slidesToShow:2,
                    slidesToScroll: 1,
                    arrows: false,
                    rtl: true,
                    responsive: [
                        {
                            breakpoint: 768,
                            settings: {
                                slidesToShow: 4,
                                slidesToScroll: 1,
                                arrows: true
                            }
                        }
                    ]
                });

                return;
            }
        });
        return $['mage']['relatedProducts'];
    };

});